<?php
require_once realpath(__DIR__.'/..').'/vendor/autoload.php';

# Noraの初期化
Nora::initialize(__DIR__, 'devel');


# 各モジュールの設定
Nora::ModuleLoader( )->on('moduleloader.loadmodule',function($e) {

    if ($e->name === 'logging')
    {
        // ロガーの設定をする
        $e->module->configure([
            'loggers' => [
                '_default' => [
                    [
                        'type' => 'stdout',
                    ]
                ]
            ]
        ]);
        $e->module->register();
    }
});

