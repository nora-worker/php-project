<?php
/**
 * プロジェクト
 */
require_once realpath(__DIR__.'/..').'/bootstrap.php';


Nora::ModuleLoader( )->load('logging', 'web');


# ロガーの適用
Nora::getLogger( )->apply(Nora::scope());

# Webを起動
Nora::Web( )
    ->run();
